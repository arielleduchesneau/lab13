
document.addEventListener("DOMContentLoaded", function () {
    var connexion = new MovieDB();
    connexion.requeteDernierFilm()


});


class MovieDB{

    constructor(){

        this.APIKey = "c18ca239fd94873e938ae598f56670ab";
        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";
        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largueurAffiche = ["92","154","185","342","500","780"];
        this.largueurTeteAffiche = ["45","185"];

        this.totalFilm = 8;
        this.totalTeteAffiche = 6;

        console.log("ca marche");

    }

    requeteDernierFilm(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourRequeteDernierFilm.bind(this));


        xhr.open("GET", this.baseURL + "movie/latest?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

    }

    retourRequeteDernierFilm(e){

        var target = e.currentTarget;
        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

           this.afficheDernierFilm(data)
        }

    }

    afficheDernierFilm(data){
        console.log(data);
    }



}